# Tahoma

## Description

This code has been made for raspberry pi. The goal is to add switches to control somfy blinds in Domoticz. Actually, all controls are opened (no authentication) in a LAN network.

> [!WARNING]
> Do not open the server port directly to Internet

## Get the code
```bash
~# git clone git@gitlab.com:domos/tahoma.git
~# cd tahoma && npm install
````

## Configurations

Create a .env file in tahoma directory and modify with your prefs:
```
tahoma_process=domos-tahoma
SERVER_PORT=7654
TAHOMA_ENDPOINT=https://www.tahomalink.com/enduser-mobile-web/enduserAPI
TAHOMA_USER=xxxxx
TAHOMA_PASSWORD=xxxxx
```

Modify the **_DIR** variable in **bin/*.sh** directory:
```
_DIR=/opt/tahoma
```

Generate SSL key and certificate:
``` bash
~# # ./bin/gen_cert.sh server
Generate the private key: server.key
Generating RSA private key, 2048 bit long modulus
..+++++
.....+++++
e is 65537 (0x10001)
Generate the CSR: server.csr
Sign the csr: server.crt
Signature ok
subject=/C=FR/ST=FRANCE/L=Paris/O=Tahoma/OU=Tahoma/CN=server/emailAddress=no-reply@domain.com
Getting Private key
'server.csr' supprimé
```

> [!INFO]
> You can chnage the DN of certificate by editing **./bin/gen_cert.sh**

## Start Server

``` bash
~# ./bin/start.sh
```

##  Access to IHM and API

* IHM: https://< ip >:< port >/ 

![IHM](assets/ihm.png)

* API: https://< ip >:< port >/api/v1/execute?deviceurl=< device url encoded>&command=< open | stop | close >
