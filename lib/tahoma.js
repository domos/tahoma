require('dotenv').config();
const signale = require('signale');
const request = require('request-promise');

const TAHOMA_ENDPOINT = process.env.TAHOMA_ENDPOINT;
let devices;

/**
 * login to TAHOMA
 * @returns boolean
 */
const login = () => {
  let options = {
    method: 'POST',
    uri: `${TAHOMA_ENDPOINT}/login`,
    form: {
      userId: process.env.TAHOMA_USER,
      userPassword: process.env.TAHOMA_PASSWORD
    },
    json: true,
    jar: true
  };

  return request(options)
    .then(function (parsedBody) {
      signale.info("Connected");
      return true;
    })
    .catch(function (error) {
      signale.error(error);
      return false;
    });
};

/**
 * Check if already connected to TAHOMA
 * @returns boolean
 */
const isAuthenticated = () => {
  let options = {
    method: 'GET',
    uri: `${TAHOMA_ENDPOINT}/authenticated`,
    json: true,
    jar: true
  };

  return request(options)
    .then((response) => {
      signale.info(`Is authenticated ? ==> ${response.authenticated}`);
      return response.authenticated;
    })
    .catch(function (error) {
      signale.error(error);
      return false;
    });
};

/**
 * Get Devices
 * @returns Object
 */
const getDevices = () => {
  let options = {
    method: 'GET',
    uri: `${TAHOMA_ENDPOINT}/setup/devices`,
    json: true,
    jar: true
  };

  return request(options)
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      signale.error(error);
    });
};

/**
 * Get Groups
 * @returns Object
 */
const getGroups = () => {
  let options = {
    method: 'GET',
    uri: `${TAHOMA_ENDPOINT}/actionGroups`,
    json: true,
    jar: true
  };

  return request(options)
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      signale.error(error);
    });
};

/**
 * Execute action
 *  @returns boolean
 */
const execute = (deviceURL, command) => {
  let options = {
    method: 'POST',
    uri: `${TAHOMA_ENDPOINT}/exec/apply`,
    json: true,
    jar: true,
    headers: {
      "Content-Type": "application/json"
    },
    body: {
      actions: [
        {
          deviceURL: deviceURL,
          commands: [{ name: command }]
        }
      ]
    }
  };

  return request(options)
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      signale.error(error);
      return false;
    });
};

/**
 * Execute group action
 *  @returns boolean
 */
const groupExecute = (oid) => {
  let options = {
    method: 'POST',
    uri: `${TAHOMA_ENDPOINT}/exec/${oid}`,
    json: true,
    jar: true,
    headers: {
      "Content-Type": "application/json"
    },
    body: {
      actionGroupOID: oid
    }
  };

  return request(options)
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      signale.error(error);
      return false;
    });
};

/**
 * logout from TAHOMA
 * @returns boolean
 */
const logout = () => {
  let options = {
    method: 'POST',
    uri: `${TAHOMA_ENDPOINT}/logout`,
    json: true,
    jar: true
  };

  return request(options)
    .then((response) => {
      if (response.logout === true)
        signale.info("Logout successfully");
      return response.logout;
    })
    .catch(function (error) {
      signale.error(error);
    });
};

module.exports = {
  login: login,
  isAuthenticated: isAuthenticated,
  getDevices: getDevices,
  getGroups: getGroups,
  execute: execute,
  groupExecute: groupExecute,
  logout: logout
};