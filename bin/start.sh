#!/bin/bash

_DIR=/opt/tahoma
cd ${_DIR}
export PM2_HOME=/tmp/.pm2

${_DIR}/node_modules/pm2/bin/pm2 start -n tahoma index.js
