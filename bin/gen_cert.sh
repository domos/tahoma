#!/bin/bash
 
test -z "$1" && { echo "Syntaxe: $(basename $0) <name>"; exit 1; }
 
NAME=${1}
 
echo "Generate the private key: ${NAME}.key"
openssl genrsa -out ${NAME}.key 2048 || exit 2
 
echo "Generate the CSR: ${NAME}.csr"
openssl req -new -sha256 -key ${NAME}.key -out ${NAME}.csr -subj "/C=FR/ST=FRANCE/L=Paris/O=Tahoma/OU=Tahoma/CN=${NAME}/emailAddress=no-reply@domain.com" || exit 3
 
echo "Sign the csr: ${NAME}.crt"
openssl x509 -req -sha256 -days 36500 -in ${NAME}.csr -signkey ${NAME}.key -out ${NAME}.crt
rm -vf ${NAME}.csr