#!/bin/bash

_DIR=/opt/tahoma
cd ${_DIR}
export PM2_HOME=/tmp/.pm2

${_DIR}/node_modules/pm2/bin/pm2 stop --disable-logs tahoma
${_DIR}/node_modules/pm2/bin/pm2 delete --disable-logs tahoma