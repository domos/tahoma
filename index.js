const https = require('https');
const express = require('express');
const fs = require('fs');
const app = express();
const tahoma = require('./lib/tahoma');
require('dotenv').config();
process.title = process.env.tahoma_process;

https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.crt')
}, app).listen(process.env.SERVER_PORT);

app.set('view engine', 'pug');

app.get('/api/v1/execute', async (req, res) => {
  if (await tahoma.isAuthenticated() === false) await tahoma.login();
  if (await tahoma.execute(req.query.deviceurl, req.query.command))
    res.json({
      result: "OK"
    });
  else res.json({
    result: "KO"
  });
});

app.get('/api/v1/groupExecute', async (req, res) => {
  if (await tahoma.isAuthenticated() === false) await tahoma.login();
  if (await tahoma.groupExecute(req.query.oid))
    res.json({
      result: "OK"
    });
  else res.json({
    result: "KO"
  });
});

app.get('/', async (req, res) => {
  if (await tahoma.isAuthenticated() === false) await tahoma.login();

  const data = await tahoma.getDevices();
  let devices = [];
  data.forEach(element => {
    if (element.uiClass === "RollerShutter") {
      devices.push({
        name: element.label,
        url: element.deviceURL,
        encoded: encodeURIComponent(element.deviceURL)
      });
    }
  });

  if (await tahoma.isAuthenticated() === false) await tahoma.login();
  const data2 = await tahoma.getGroups();
  let groups = [];
  data2.forEach(element => {
    groups.push({
      name: element.label,
      oid: element.oid
    });
  });

  let result = {
    "groups": groups,
    "devices": devices
  };
  res.render('index', result);
});